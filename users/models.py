from django.db import models

from django.contrib.auth.models import AbstractUser
from django.db import models

'''CustomUser that
extends the existing AbstractUser. 
We also include our first custom field, age, here.'''


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
